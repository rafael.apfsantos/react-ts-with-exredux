import React from "react";
import { Wrapper } from "../../components/content/Wrapper";
import { Header } from "../../components/content/Header";
import { Sidemenu } from "../../components/content/Sidemenu";
import { Body } from "../../components/content/Body";
import { Content } from "../../components/content/Content";
import { Routes } from "../core/Routes";

export class Dashboard extends React.Component {
  render() {
    return (
      <Wrapper>         
        <Header></Header>
        <Body>
          <Sidemenu />
          <Content>
            <Routes />
          </Content>
        </Body>
      </Wrapper>
    );
  }
}
