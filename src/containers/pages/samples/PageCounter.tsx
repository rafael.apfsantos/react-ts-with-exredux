import React from "react";
import { Wrapper } from "../../../components/content/Wrapper";
import { Typography, Divider } from "@material-ui/core";
import {Reveal, Animation} from 'react-genie';
import { Counter } from "../../../components/samples/Counter";
import { CounterModel } from "../../../core/models/CounterModel";
import { Inject, Connection } from "exredux";
class ModelProps {
  @Inject counterModel: CounterModel;
}

type Props = Partial<ModelProps>; // todas as propriedades da classe ModelProps se tornam opcional

@Connection(ModelProps)
export class PageCounter extends React.Component<Props> {
  
  componentDidMount() {
    this.props.counterModel.reset();
  }

  
  render() {
    return (
      <Wrapper>
        <Reveal animation={Animation.SlideInLeft}>
          <Typography variant="h6" component="h2">
            Counter
          </Typography>
          <Divider />
          <Counter counterModel={this.props.counterModel}  />          
        </Reveal>
      </Wrapper>

      //     <CircularProgress />
      // )
    );
  }
}
