import { ProductItemModel } from "../../../core/models/ProductItemModel";
import { Inject, Connection } from "exredux";
import React from "react";
import { Wrapper } from "../../../components/content/Wrapper";
import { Reveal, Animation } from "react-genie";
import { Typography, Divider, CircularProgress } from "@material-ui/core";
import { ProductItem } from "../../../components/samples/ProductItem";
import { Failed } from "../../../components/content/Failed";
import { RouteComponentProps } from 'react-router';
import { IProductItemParams } from "../../../core/interfaces/IProductItemParams";

class ModelProps {
  @Inject productItemModel: ProductItemModel;
}

type Props = Partial<ModelProps> & RouteComponentProps<IProductItemParams>;

@Connection(ModelProps)
export class PageProductItem extends React.Component<Props> {

  componentDidMount() {
    // const { props  } = this;
    // const id = (props as any).match.params.id;
    const id = this.props.match.params.id;
    this.props.productItemModel.getById(+id);
   
  }
  render() {
    const { productItemModel } = this.props;
    return (
      <Wrapper>
        <Reveal animation={Animation.SlideInLeft}>
          <Typography variant="h6" component="h2">
            Products
          </Typography>
          <Divider />
          {productItemModel.isLoading ? <CircularProgress /> : null}
          {productItemModel.isFailed ? <Failed /> : null}
          {productItemModel.isCompleted ? (
            <ProductItem product={productItemModel.response.data} />
          ) : null}
        </Reveal>
      </Wrapper>
    );
  }
}
