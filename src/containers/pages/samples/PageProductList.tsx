import React from "react";
import { Inject, Connection } from "exredux";
import { ProductListModel } from "../../../core/models/ProductListModel";
import { CircularProgress, Typography, Divider } from "@material-ui/core";
import { ProductListTable } from "../../../components/samples/ProductListTable";
import { Failed } from "../../../components/content/Failed";
import { Wrapper } from "../../../components/content/Wrapper";
import { Reveal, Animation } from "react-genie";
class ModelProps {
  @Inject productListModel: ProductListModel;
}

type Props = Partial<ModelProps>; // todas as propriedades da classe ModelProps se tornam opcional

@Connection(ModelProps)
export class PageProductList extends React.Component<Props> {
  componentDidMount() {
    const { productListModel } = this.props;
    productListModel.getAll();
  }

  render() {
    const { productListModel } = this.props;
    return (
      <Wrapper>
        <Reveal animation={Animation.SlideInLeft}>
          <Typography variant="h6" component="h2">
            Products
          </Typography>
          <Divider />
          {productListModel.isLoading ? <CircularProgress /> : null}
          {productListModel.isFailed ? <Failed /> : null}
          {productListModel.isCompleted ? (
            <ProductListTable products={productListModel.response.data} />
          ) : null}
        </Reveal>
      </Wrapper>
    );
  }
}
