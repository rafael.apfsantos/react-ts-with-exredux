import React from "react";
import { Wrapper } from "../../../components/content/Wrapper";
import { Typography, Divider } from "@material-ui/core";
import {Reveal, Animation} from 'react-genie';
import { Inject, Connection } from "exredux";
import { CalculatorModel } from "../../../core/models/CalculatorModel";
import { Calculator } from "../../../components/samples/Calculator";
class ModelProps {
  @Inject calculatorModel: CalculatorModel;
}

type Props = Partial<ModelProps>; // todas as propriedades da classe ModelProps se tornam opcional

@Connection(ModelProps)
export class PageCalculator extends React.Component<Props> {
  
  componentDidMount() {
    this.props.calculatorModel.reset();
  }

  
  render() {
    return (
      <Wrapper>
        <Reveal animation={Animation.SlideInLeft}>
          <Typography variant="h6" component="h2">
            Calculator
          </Typography>
          <Divider />
          <Calculator calculatorModel={this.props.calculatorModel}  />          
        </Reveal>
      </Wrapper>
    );
  }
}
