import * as React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { PageDashboard } from '../pages/main/PageDashboard';
import { PageCounter } from '../pages/samples/PageCounter';
import { PageCalculator } from '../pages/samples/PageCalculator';
import { PageProductList } from '../pages/samples/PageProductList';
import { PageProductItem } from '../pages/samples/PageProductItem';

export class Routes extends React.Component {
  public render() {
    return (
      <React.Fragment>
        {/* <RouterHandler /> */}
        <Switch>
          <Route exact={true} path="/" component={PageDashboard} />
          <Route exact={true} path="/counter" component={PageCounter} />
          <Route exact={true} path="/calculator" component={PageCalculator} />
          <Route exact={true} path="/products" component={PageProductList} />
          <Route exact={true} path="/product/:id" component={PageProductItem} />
          <Redirect to="/" />
        </Switch>
      </React.Fragment>
    );
  }
}
