import * as React from "react";
import { createJssStyle } from "../../helpers/styler";
const logo = require("../../assets/images/xpi-black.jpg").default;
// ---------------------------------------------------------------------
// --- STYLE -----------------------------------------------------------
// ---------------------------------------------------------------------
const { classes } = createJssStyle({
  pageHeader: {
    background: "#000",
    color: "#fff",
    padding: "16px",
    display: "flex",
    alignContent: "center"
  },
  imgLogo: {
    maxHeight: "25px",
    marginRight: "10px"
  }
});
// ---------------------------------------------------------------------
// --- COMPONENT -------------------------------------------------------
// ---------------------------------------------------------------------
export class Header extends React.Component {
  render() {
    return (
      <div className={classes.pageHeader}>
        <img className={classes.imgLogo} src={logo} />
        {this.props.children}
      </div>
    );
  }
}
