import * as React from "react";
import { createJssStyle } from "../../helpers/styler";
import Alert from "@material-ui/lab/Alert";
// ---------------------------------------------------------------------
// --- STYLE -----------------------------------------------------------
// ---------------------------------------------------------------------
const { classes } = createJssStyle({
  failed: {
    width: "100%",
    textAlign: "center"
  }
});
// ---------------------------------------------------------------------
// --- COMPONENT -------------------------------------------------------
// ---------------------------------------------------------------------
export class Failed extends React.Component {
  render() {
    return (
      <div className={classes.failed}>
        <Alert severity="error">Failed to request</Alert>
      </div>
    );
  }
}
