import * as React from "react";
import { createJssStyle } from "../../helpers/styler";
import { Link } from "react-router-dom";
import {
  List,
  ListItem,
  ListSubheader,
  ListItemIcon,
  ListItemText
} from "@material-ui/core";
import AppsIcon from "@material-ui/icons/Apps";
import BallotIcon from "@material-ui/icons/Ballot";
import ListIcon from "@material-ui/icons/List";
import TollIcon from "@material-ui/icons/Toll";
import { fontWeight } from "@material-ui/system";

// ---------------------------------------------------------------------
// --- STYLE -----------------------------------------------------------
// ---------------------------------------------------------------------
const { classes } = createJssStyle({
  sidemenu: {
    backgroundColor: "#f1f3f4",
    padding: "10px",
    margin: 0
  },
  itemMenu: {
    display: "flex",
    alignContent: "center",
    color: 'rgba(0, 0, 0, 0.54)',
    fontWeight: 'bold',
    textDecoration: 'none'
  }
});
// ---------------------------------------------------------------------
// --- COMPONENT -------------------------------------------------------
// ---------------------------------------------------------------------
export class Sidemenu extends React.Component {
  render() {
    return (
      <div className={classes.sidemenu}>
        <List
          component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={
            <ListSubheader component="div" id="nested-list-subheader">
              Menu
            </ListSubheader>
          }
        >
          <ListItem button>
            <Link to="/" className={classes.itemMenu}>
              <ListItemIcon>
                <AppsIcon />
              </ListItemIcon>
              <span>Dashboard </span>
            </Link>
          </ListItem>
          <ListItem button>
            <Link to="/counter" className={classes.itemMenu}>
              <ListItemIcon>
                <TollIcon />
              </ListItemIcon>
              <span>Counter </span>
            </Link>
          </ListItem>
          <ListItem button>
            <Link to="/calculator" className={classes.itemMenu}>
              <ListItemIcon>
                <BallotIcon />
              </ListItemIcon>
              <span>Calculator </span>
            </Link>
          </ListItem>
          <ListItem button>
            <Link to="/products" className={classes.itemMenu}>
              <ListItemIcon>
                <ListIcon />
              </ListItemIcon>
              <span>Products </span>
            </Link>
          </ListItem>
        </List>
      </div>
      // <ul >
      //   <li><Link to="/">Dashboard</Link></li>
      //   <li><Link to="/products">Products</Link></li>
      //   <li><Link to="/contact">Contact</Link></li>
      // </ul>
    );
  }
}
