import * as React from "react";
import { createJssStyle } from "../../helpers/styler";
import { Link } from "react-router-dom";
import { TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Paper } from "@material-ui/core";
import { IProductsResult } from "../../core/interfaces/IProductsResult";
// ---------------------------------------------------------------------
// --- STYLE -----------------------------------------------------------
// ---------------------------------------------------------------------
const { classes } = createJssStyle({
  table: {
    width: '100% !important'  
  }
});
// ---------------------------------------------------------------------
// --- PROPS -------------------------------------------------------
// ---------------------------------------------------------------------
interface Props {
  products: IProductsResult;
}
// ---------------------------------------------------------------------
// --- COMPONENT -------------------------------------------------------
// ---------------------------------------------------------------------
export class ProductListTable extends React.Component<Props> {
  render() {
    const { products } = this.props;
    return (
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="Table products">
          <TableHead>
            <TableRow>
              <TableCell>ID</TableCell>
              <TableCell align="right">Name</TableCell>
              <TableCell align="right">Color</TableCell>
              <TableCell align="right">Options</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {products.data.map((row, index) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.id}
                </TableCell>
                <TableCell align="right">{row.name}</TableCell>
                <TableCell align="right">{row.color}</TableCell>
                <TableCell align="right"> <Link to={`/product/${row.id}`}>See detail</Link></TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>

      //   <table className={classes.table}>
      //     <thead>
      //       <tr>
      //         <th>ID</th>
      //         <th>Name</th>
      //         <th>Color</th>
      //       </tr>
      //     </thead>
      //     <tbody>
      //       {products.data.map((item, index) => (
      //         <tr key={index}>
      //           <td>
      //             {item.id}
      //           </td>
      //           <td>{item.name}</td>
      //           <td>
      //             <div className={classes.box} style={{ backgroundColor: item.color }} />
      //           </td>
      //           <td>
      //             <Link to={`/item/${item.id}`}>See detail</Link>
      //           </td>
      //         </tr>
      //       ))}
      //     </tbody>
      //   </table>
    );
  }
}
