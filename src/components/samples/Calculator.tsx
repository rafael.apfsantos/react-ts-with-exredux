import * as React from "react";
import { createJssStyle } from "../../helpers/styler";
import {
  Card,
  CardContent,
  Typography,
  Button,
  CardHeader,
  TextField
} from "@material-ui/core";
import { Wrapper } from "../content/Wrapper";
import { ICalculator } from "../../core/interfaces/ICalculator";
import Alert from "@material-ui/lab/Alert";
// ---------------------------------------------------------------------
// --- STYLE -----------------------------------------------------------
// ---------------------------------------------------------------------
const { classes } = createJssStyle({
  content: {
    padding: "32px"
  },
  root: {
    minWidth: 275,
    display: "flex",
    justifyContent: "center"
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
});
// ---------------------------------------------------------------------
// --- COMPONENT -------------------------------------------------------
// ---------------------------------------------------------------------
export class Calculator extends React.Component<ICalculator> {
  componentDidUpdate() {
    console.log("Last Action: ", this.props.calculatorModel.lastAction);
    console.log("Result: ", this.props.calculatorModel.result);
  }

  handleFieldUpdate = (fieldName: string) => evt => {
    this.props.calculatorModel.doFieldUpdate(fieldName, evt.target.value);
  };

  render() {
    const { calculatorModel } = this.props;
    return (
      <React.Fragment>
        <Wrapper>
          <Card className={classes.root}>
            <CardContent>
              <Typography color="textSecondary" variant="h4" gutterBottom>
                {calculatorModel.result}
              </Typography>
              <form className={classes.root} noValidate autoComplete="off">
                <TextField
                  id="number_1"
                  label="Number 1"
                  variant="outlined"
                  placeholder="Value 1"
                  onChange={this.handleFieldUpdate("number_1")}
                  value={calculatorModel.number_1}
                />
                <TextField
                  id="number_2"
                  label="Number 2"
                  variant="outlined"
                  onChange={this.handleFieldUpdate("number_2")}
                  value={calculatorModel.number_2}
                  placeholder="Value 2"
                />
              </form>
              <hr />
              <label htmlFor="outlined-button-plus">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={calculatorModel.addition}
                >
                  +
                </Button>
              </label>
              <label htmlFor="outlined-button-plus">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={calculatorModel.subtraction}
                >
                  -
                </Button>
              </label>
              <label htmlFor="outlined-button-plus">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={calculatorModel.multiplication}
                >
                  *
                </Button>
              </label>
              <label htmlFor="outlined-button-plus">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={calculatorModel.division}
                >
                  /
                </Button>
              </label>
              <label htmlFor="outlined-button-plus">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={calculatorModel.reset}
                >
                  C
                </Button>
              </label>
              {calculatorModel.messages.length > 0 ? (
                <div className={classes.root}>
                  {calculatorModel.messages.map((item, index) => (
                    <Alert key={index} severity={item.severity}>{item.message}</Alert>
                  ))}
                </div>
              ) : null}
            </CardContent>
          </Card>
        </Wrapper>
      </React.Fragment>
    );
  }
}
