import * as React from "react";
import { createJssStyle } from "../../helpers/styler";
import { ICounter } from "../../core/interfaces/ICounter";
import { Card, CardContent, Typography, Button } from "@material-ui/core";
import { Wrapper } from "../content/Wrapper";
// ---------------------------------------------------------------------
// --- STYLE -----------------------------------------------------------
// ---------------------------------------------------------------------
const { classes } = createJssStyle({
  content: {
    padding: "32px"
  },
  root: {
    minWidth: 275,
    display:'flex',
    justifyContent: 'center'
  },
  bullet: {
    display: "inline-block",
    margin: "0 2px",
    transform: "scale(0.8)"
  },
  title: {
    fontSize: 14
  },
  pos: {
    marginBottom: 12
  }
});
// ---------------------------------------------------------------------
// --- COMPONENT -------------------------------------------------------
// ---------------------------------------------------------------------
export class Counter extends React.Component<ICounter> {
  render() {
    const { counterModel } = this.props;
    return (
      <React.Fragment>
        <Wrapper>
          <Card className={classes.root}>
            <CardContent>
              <Typography color="textSecondary" variant="h4" gutterBottom>
                {counterModel.counter}
              </Typography>
              <label htmlFor="outlined-button-plus">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={counterModel.add}
                >
                  ADD
                </Button>
              </label>
              <label htmlFor="outlined-button-file">
                <Button
                  variant="outlined"
                  component="span"
                  onClick={counterModel.del}
                >
                  REMOVE
                </Button>
              </label>
              { (counterModel.lastAction != '') ? (
                <Typography color="textSecondary" variant="h6" gutterBottom>
                  Last Action = {counterModel.lastAction}
                </Typography>
              ) : null}
            </CardContent>
          </Card>
        </Wrapper>
      </React.Fragment>
    );
  }
}
