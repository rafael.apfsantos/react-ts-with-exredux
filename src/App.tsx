import * as React from "react";
import { HashRouter } from "react-router-dom";
import { Dashboard } from "./containers/content/Dashboard";
import { Provider } from "exredux";
import { CounterModel } from "./core/models/CounterModel";
import { CalculatorModel } from "./core/models/CalculatorModel";
import { ProductListModel } from "./core/models/ProductListModel";
import { ProductItemModel } from "./core/models/ProductItemModel";

export class App extends React.Component {
  public render() {
    return (
      <HashRouter>
        <Provider models={[CounterModel, CalculatorModel, ProductListModel, ProductItemModel]}>
          <Dashboard />
        </Provider>
      </HashRouter>
    );
  }
}
