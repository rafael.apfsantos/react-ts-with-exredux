import { http } from 'exredux';
import { IProductResult } from "../interfaces/IProductResult";
import { IProductsResult } from '../interfaces/IProductsResult';
const API_URL = process.env.API_URL;

export class ProductRepository {

    static ENDPOINT_GET_ALL = `${API_URL}/api/unknown`;
    static ENDPOINT_GET_PRODUCT = `${API_URL}/api/unknown/{id}`;

    public static getAll() {
        return http.get<IProductsResult>(this.ENDPOINT_GET_ALL);
    }

    public static getById(id: number) {
        return http.get<IProductResult>(this.ENDPOINT_GET_PRODUCT.replace('{id}', id.toString()));
    }

}