import { IMessages } from "./IMessages";

export interface IJsonDataResult<T> {
    data: T[] | T,
    error: boolean,
    messages: IMessages[]
}