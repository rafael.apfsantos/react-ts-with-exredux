import { CalculatorModel } from "../models/CalculatorModel";

export interface ICalculator {
    calculatorModel: CalculatorModel
}