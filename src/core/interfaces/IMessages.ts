import { Color } from "@material-ui/lab/Alert";

export interface IMessages {
    severity?: Color;
    message: string;
}