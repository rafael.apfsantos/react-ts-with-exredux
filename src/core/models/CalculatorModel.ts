import { Action, Trigger } from 'exredux';
import { IMessages } from '../interfaces/IMessages';
import { Color } from '@material-ui/lab/Alert';

export class CalculatorModel {
    public number_1: number = 0;
    public number_2: number = 0;
    public result: number = 0;
    public lastAction: string = '';
    public messages: Array<IMessages> = new Array<IMessages>();
    public errors: boolean = false;

    @Action addition() {
        this._cleanMessages();      
        this.result = this.number_1 + this.number_2;
    }

    @Action subtraction() {
        this._cleanMessages();
        this.result = this.number_1 - this.number_2;
    }

    @Action multiplication() {
        this._cleanMessages();
        this.result = this.number_1 * this.number_2;
    }

    @Action division() {
        this._cleanMessages();
        this.result = 0;
        this._validateDivisionByZero();
        if (!this.errors) {
            this.result = this.number_1 / this.number_2;
        }
    }

    @Trigger('addition')
    lastActionAddition() {
        this.lastAction = `addition ${this.number_1} + ${this.number_2} = ${this.result}`;
    }

    @Trigger('subtraction')
    lastActionSubtraction() {
        this.lastAction = `subtraction ${this.number_1} - ${this.number_2} = ${this.result}`;
    }

    @Trigger('multiplication')
    lastActionMultiplication() {
        this.lastAction = `multiplication ${this.number_1} * ${this.number_2} = ${this.result}`;
    }

    @Trigger('division')
    lastActionDivision() {        
        this.lastAction = `division ${this.number_1} / ${this.number_2} = ${this.result}`;
    }

    @Action
    doFieldUpdate(fieldName: string, value: number) {
        this[fieldName] = +value;
    }

    @Action reset() {
        this.result = 0;
        this.number_1 = 0;
        this.number_2 = 0;
        this.lastAction = '';
        this.messages = [];
        this.errors = false;
    }

    private _validateDivisionByZero() {
        if (this.number_2 == 0) {
            this._setMessage('error', 'Unable to divide by 0');
            this.errors = true;
        }       
    }

    private _setMessage(severity: Color, message: string) {
        this.messages.push({severity: severity, message: message});
    }

    private _cleanMessages() {
        this.messages = [];
        this.errors = false;
    }
}
