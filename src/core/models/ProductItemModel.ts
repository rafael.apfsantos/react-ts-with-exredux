import { Action, BaseHttpModel } from 'exredux';
import { ProductRepository } from '../repositories/ProductRepository';
import { IProductResult } from '../interfaces/IProductResult';
import { RouteComponentProps } from 'react-router';
interface ProductRouteParams {
    id: string;
  }
export class ProductItemModel extends BaseHttpModel<IProductResult> {

    public router: RouteComponentProps<ProductRouteParams>;

    @Action
    public getById(id: number) {
        this.request(ProductRepository.getById(id));
    }    
} 
