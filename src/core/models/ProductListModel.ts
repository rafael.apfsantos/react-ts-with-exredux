import { Action, BaseHttpModel } from 'exredux';
import { ProductRepository } from '../repositories/ProductRepository';
import { IProductsResult } from '../interfaces/IProductsResult';

export class ProductListModel extends BaseHttpModel<IProductsResult> {
    @Action
    public getAll() {
        this.request(ProductRepository.getAll());
    }    
} 
